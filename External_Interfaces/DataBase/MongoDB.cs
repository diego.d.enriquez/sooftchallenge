﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;

namespace SooftChallenge.External_Interfaces.DataBase
{
    public class MongoDataBase
    {
        private MongoClient _client = new MongoClient("mongodb+srv://user_test:usr77@cluster0.qwrpd.mongodb.net/sooftchallenge?retryWrites=true&w=majority");
        
        public List<Promocion> getPromociones(){
            var database = _client.GetDatabase("sooftchallenge");
            var collection = database.GetCollection<Promocion>("promocion");

            return IMongoCollectionExtensions
            .AsQueryable(collection).ToList<Promocion>();
        }

        public Guid NewPromocion( Promocion promocion)
        {
            var database = _client.GetDatabase("sooftchallenge");
            var collection = database.GetCollection<Promocion>("promocion");

            collection.InsertOne(promocion);

            return promocion.Id;
        }

        public Guid UpdatePromocion(Promocion promocion)
        {
            var database = _client.GetDatabase("sooftchallenge");
            var collection = database.GetCollection<Promocion>("promocion");

            collection.ReplaceOneAsync( p => p.Id == promocion.Id, promocion );

            return promocion.Id;
        }

        public Guid DeletePromocion(Guid id)
        {
            var database = _client.GetDatabase("sooftchallenge");
            var collection = database.GetCollection<Promocion>("promocion");


            var promocion = collection.Find<Promocion>(p => p.Id == id).FirstOrDefault();

            promocion.Activo = false;

            collection.ReplaceOneAsync(p => p.Id == promocion.Id, promocion);

            return promocion.Id;
        }

        public Guid UpdateVigenciaPromocion(Guid id, DateTime? fechaInicio, DateTime? fechaFin)
        {
            var database = _client.GetDatabase("sooftchallenge");
            var collection = database.GetCollection<Promocion>("promocion");


            var promocion = collection.Find<Promocion>(p => p.Id == id).FirstOrDefault();

            promocion.FechaInicio = fechaInicio;
            promocion.FechaFin = fechaFin;

            collection.ReplaceOneAsync(p => p.Id == promocion.Id, promocion);

            return promocion.Id;
        }
    }
}

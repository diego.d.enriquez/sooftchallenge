﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SooftChallenge.External_Interfaces.DataBase;
using SooftChallenge.Bussines_Validations;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SooftChallenge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PromocionController : ControllerBase
    {
        // GET: api/<PromocionController>
        [HttpGet]
        public IEnumerable<Promocion> Get()
        {
            var db = new MongoDataBase();
            var promociones = db.getPromociones();

            return promociones;
        }
        
        [HttpGet("{id}")]
        public Promocion Get(Guid id)
        {
            var db = new MongoDataBase();
            var promociones = db.getPromociones();

            return promociones.Find( p => p.Id.Equals(id));
        }

        [HttpPost]
        public IActionResult Post([FromBody] Promocion promocion)
        {
            var validator = new PromocionValidation();

            var validations = validator.Validate(promocion);

            if( validations.Count > 0 ) return Problem("No Valida Reglas de Negocio");

            var db = new MongoDataBase();
            
            var guid = new Guid();

            if ( promocion.Id.ToString() == "00000000-0000-0000-0000-000000000000")
                guid = db.NewPromocion(promocion);
            else
                guid = db.UpdatePromocion(promocion);
            
            return Ok(guid);
        }

        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            var db = new MongoDataBase();
            db.DeletePromocion(id);
        }
    }
}

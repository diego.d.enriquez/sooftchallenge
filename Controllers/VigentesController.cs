﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SooftChallenge.External_Interfaces.DataBase;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SooftChallenge.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VigentesController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<Promocion> Get()
        {
            var db = new MongoDataBase();
            var promociones = db.getPromociones();

            return promociones.FindAll(p => p.FechaInicio < DateTime.Now && p.FechaFin > DateTime.Now);
        }

        [HttpGet("{fecha}")]
        public IEnumerable<Promocion> Get(DateTime fecha)
        {
            var db = new MongoDataBase();
            var promociones = db.getPromociones();

            return promociones.FindAll(p => p.FechaInicio < fecha && p.FechaFin > fecha);
        }

        [HttpPost]
        public IActionResult Post([FromQuery] Guid id, DateTime? fechaInicio, DateTime? fechaFin)
        {
            var db = new MongoDataBase();

            var guid = db.UpdateVigenciaPromocion(id, fechaInicio, fechaFin);

            return Ok(guid);

        }
    }
}

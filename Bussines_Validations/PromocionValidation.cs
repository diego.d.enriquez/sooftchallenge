﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SooftChallenge.External_Interfaces.DataBase;

namespace SooftChallenge.Bussines_Validations
{
    public class PromocionValidation
    {
        public List<string> Validate(Promocion promocion)
        {
            var validations = new List<string>();

            var db = new MongoDataBase();
            var promociones = db.getPromociones().FindAll(p => p.FechaInicio < DateTime.Now && p.FechaFin > DateTime.Now);

            var interseccionMediosDePago = promociones.FindAll(p => p.MediosDePago != null && p.MediosDePago.Intersect(promocion.MediosDePago).Any());
            var interseccionBanco = promociones.FindAll(p => p.Bancos != null && p.Bancos.Intersect(promocion.Bancos).Any());
            var interseccionCategoria = promociones.FindAll(p => p.CategoriasProductos != null && p.CategoriasProductos.Intersect(promocion.CategoriasProductos).Any());

            if (interseccionMediosDePago.Any() && interseccionBanco.Any() && interseccionCategoria.Any())
                validations.Add("Error de Negocio : Existe una promoción para Medio de Pago / Banco / Categoría.");

            if(promocion.MaximaCantidadDeCuotas == null && promocion.PorcentajeDeDescuento == null)
                validations.Add("Error de Negocio : Cantidad de Cuotas y Porcentaje de Descuentos sin valor.");

            if( promocion.ValorInteresCuotas != null && promocion.MaximaCantidadDeCuotas == null)
                validations.Add("Error de Negocio : Interes de Cuota sin Cntidad de Cuotas");

            if( promocion.PorcentajeDeDescuento != null && (promocion.PorcentajeDeDescuento < 5 || promocion.PorcentajeDeDescuento > 80) )
                validations.Add("Error de Negocio : Porcentaje de Descuento Fuera de Rango");

            if( promocion.FechaFin < promocion.FechaInicio )
                validations.Add("Error de Negocio : Fecha Fin mayor a Fecha Inicio");

                return validations;
        }
    }
}
